import sys
def decode():
    keys = []
    for length in range(1,8):
        num = 2 ** length - 1
        for i in range(num):
            key = bin(i)[2:].zfill(length)
            keys.append(key)

    while True:
        header = sys.stdin.readline().strip('\r\n')
        if not header:
            break
        key_map={}
        for i,c in enumerate(header):
            key = keys[i]
            key_map[key] = c
            encode_msg = ""
        def read_msg(num):
            nonlocal encode_msg
            while len(encode_msg) < num:
                  encode_msg += sys.stdin.readline().strip()
            msg = encode_msg[:num]
            encode_msg = encode_msg[num:]
            return msg

        decode_msg = ""
        while True:
            key_len = int(read_msg(3),2)
            if key_len == 0:
                break
            while True:
                key = read_msg(key_len)
                if key not in key_map:
                    break
                decode_msg +=key_map[key]
    print(decode_msg)

if __name__ == '__main__':
    decode()
                                                                                                                                                               1,1         


